import tensorflow as tf
from math import pi
import numpy as np


# TODO: move to Tensorflow 2 to use the real function
def is_tensor(x):
    return isinstance(x, tf.Tensor)


def fspecial(filtertype, optionalparams):
    if filtertype == "average":
        sze = optionalparams
        if is_tensor(sze):
            x = tf.cast(sze, dtype=tf.int32)
            x = sze.get_shape().as_list()
            if len(x) == 2:
                rows, cols = sze[0], sze[1]
                num = tf.cast(rows * cols, dtype=tf.float32)
                return tf.ones((rows, cols)) / num
            elif len(x) == 1:
                rows, cols = sze[0], sze[0]
                num = tf.cast(rows * cols, dtype=tf.float32)
                return tf.ones((rows, cols)) / num
            else:
                rows, cols = sze, sze
                num = tf.cast(rows * cols, dtype=tf.float32)
                return tf.ones((rows, cols)) / num
        else:
            rows, cols = sze, sze
            return tf.ones((rows, cols)) / (rows * cols)


# Performs 2D convolution similarly to Matlab's conv2 using Tensorflow
# If you experience errors, see
# https://github.com/tensorflow/tensorflow/issues/11864
def conv2(a, b, mode="same"):
    if len(a.shape) == 2:
        rows, cols = a.shape
        a = tf.reshape(a, [1, rows, cols, 1])
    if len(b.shape) == 2:
        rows, cols = b.shape
        b = b[::-1, ::-1]
        b = tf.reshape(b, [rows, cols, 1, 1])
    convolved = tf.nn.conv2d(a, b, strides=[1, 1, 1, 1], padding=mode.upper())
    return tf.squeeze(convolved)


def fft2(X, m=None, n=None):
    """Mocks Matlab's 2-D fast Fourier transform

    see: https://www.mathworks.com/help/matlab/ref/fft2.html

    Parameters:
    X (Tensor): Input array, specified as a matrix or a multidimensional 
                Tensorflow tensor of 2D dimensions.
    m (uint):   Number of transform rows
    n (uint):   Number of transform columns

    Returns:
    Complex Tensor: Returns the two-dimensional Fourier transform of a matrix
    """
    if m == None or n == None:
        m, n = X.shape
    if X.dtype != tf.complex64 and X.dtype != tf.complex128:
        X = tf.cast(X, dtype=tf.complex64)
    return tf.signal.fft2d(X)


def ifft2(X, m=None, n=None):
    """Mocks Matlab's 2-D inverse fast Fourier transform

    see: https://www.mathworks.com/help/matlab/ref/fft2.html

    Parameters:
    X (Tensor): Input array, specified as a matrix or a multidimensional 
                Tensorflow tensor of 2D dimensions.
    m (uint):   Number of transform rows
    n (uint):   Number of transform columns

    Returns:
    Complex Tensor: Returns the two-dimensional Fourier transform of a matrix
    """
    if m == None or n == None:
        m, n = X.shape
    if X.dtype != tf.complex64 and X.dtype != tf.complex128:
        X = tf.cast(X, dtype=tf.complex64)
    return tf.signal.ifft2d(X)


def lowpassfilter(sze, cutoff, n):
    """ Constructs a low-pass butterworth filter.

    Parameters:
    sze (tuple):     is a two element vector specifying the size of filt 
                     to construct [rows cols].
    cutoff (double): cutoff frequency of the filter 0 - 0.5
    n (uint):        Order of the filter, the higher n is the sharper the
                     the transition is. (n must be an integer >= 1)
                     n is doubled so that it is always an even integer.

    Returns:
    Complex Tensor: Returns the two-dimensional Fourier transform of a matrix
    """
    if cutoff < 0 or cutoff > 0.5:
        raise ValueError('cutoff frequency must be between 0 and 0.5')
    if n % 1 != 0 or n < 1:
        raise ValueError('n must be an integer >= 1')
    rows, cols = (sze, sze) if len(sze) == 1 else (sze[0], sze[1])
    xrange, yrange = None, None
    if cols % 2:
        xrange = tf.range(-(cols-1)/2, cols/2, dtype=tf.float32) / (cols-1)
    else:
        xrange = tf.range(-cols/2, cols/2, dtype=tf.float32)/cols
    if rows % 2:
        yrange = tf.range(-(rows-1)/2, rows/2, dtype=tf.float32) / (rows-1)
    else:
        yrange = tf.range(-rows/2, rows/2, dtype=tf.float32)/rows
    x, y = tf.meshgrid(xrange, yrange)
    radius = tf.sqrt(x * x + y * y)
    f = tf.signal.ifftshift(1 / (1.0 + (radius / cutoff)**(2*n)))
    return f


def _update_tf_variable(variable, value):
    return variable.assign(value)


def median(s):
    v = tf.reshape(s, [-1])
    l = v.get_shape()[0]
    mid = l//2 + 1
    val = tf.nn.top_k(v, mid).values
    if l % 2 == 1:
        return val[-1]
    else:
        return 0.5 * (val[-1] + val[-2])


def phasecong2(im):
    nscale = 4     # Number of wavelet scales.
    norient = 4     # Number of filter orientations.
    minWaveLength = 6     # Wavelength of smallest scale filter.
    mult = 2     # Scaling factor between successive filters.
    sigmaOnf = 0.55  # Ratio of the standard deviation of the
    # Gaussian describing the log Gabor filter's
    # transfer function in the frequency domain
    # to the filter center frequency.
    dThetaOnSigma = 1.2   # Ratio of angular interval between filter orientations
    # and the standard deviation of the angular Gaussian
    # function used to construct filters in the
    # freq. plane.
    k = 2.0   # No of standard deviations of the noise
    # energy beyond the mean at which we set the
    # noise threshold point.
    # below which phase congruency values get
    # penalized.
    epsilon = .0001  # Used to prevent division by zero.

    thetaSigma = pi/norient/dThetaOnSigma  # Calculate the standard deviation of
    # the angular Gaussian function used
    # to construct filters in the
    # frequency plane.

    rows, cols = im.get_shape().as_list()
    imagefft = fft2(im)
    zero = tf.zeros((rows, cols))
    EO = [[None] * nscale for i in range(norient)]
    ifftFilterArray = 20 * [None]
    if cols % 2:
        xrange = tf.range(-(cols-1)/2, cols/2, dtype=tf.float32) / (cols-1)
    else:
        xrange = tf.range(-cols/2, cols/2, dtype=tf.float32)/cols
    if rows % 2:
        yrange = tf.range(-(rows-1)/2, rows/2, dtype=tf.float32) / (rows-1)
    else:
        yrange = tf.range(-rows/2, rows/2, dtype=tf.float32)/rows
    x, y = tf.meshgrid(xrange, yrange)
    radius = tf.sqrt(x * x + y * y)
    radius = tf.signal.ifftshift(radius)
    radius = tf.Variable(radius)
    radius = radius[0, 0].assign(1)
    theta = tf.math.atan2(-y, x)
    theta = tf.signal.ifftshift(theta)
    sintheta = tf.math.sin(theta)
    costheta = tf.math.cos(theta)
    del x, y, theta
    lp = lowpassfilter([rows, cols], .45, 15)
    logGabor = []
    for s in range(nscale):
        wavelength = minWaveLength * mult ** s
        fo = 1.0 / wavelength
        band = tf.exp((-(tf.log(radius/fo))**2) / (2 * tf.log(sigmaOnf)**2))
        tensor = lp * band
        # TODO: make it with no numpy
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            tensor = tensor.eval(session=sess)
            tensor[0, 0] = 0
            tensor = tf.convert_to_tensor(tensor)
        logGabor.append(tensor)

    spread = []
    for o in range(norient):
        angle = o * pi / norient
        ds = sintheta * tf.math.cos(angle) - costheta * tf.math.sin(angle)
        dc = costheta * tf.math.cos(angle) + sintheta * tf.math.sin(angle)
        dtheta = tf.math.abs(tf.math.atan2(ds, dc))
        angular_filter = tf.math.exp((-dtheta ** 2) / (2 * thetaSigma ** 2))
        spread.append(angular_filter)

    # with tf.Session() as sess:
    #     sess.run(tf.global_variables_initializer())
    #     print(sess.run(spread))
    #     print("spread-=", type(spread))

    # The mail loop
    EnergyAll = tf.zeros((rows, cols))
    AnAll = tf.zeros((rows, cols))
    EM_n = None
    for o in range(norient):
        sumE_ThisOrient = zero
        sumO_ThisOrient = zero
        sumAn_ThisOrient = zero
        Energy = zero
        for s in range(nscale):
            filter = logGabor[s][:, :] * spread[o][:, :]
            ifftFilt = tf.math.real(ifft2(filter)) * \
                tf.math.sqrt(1.0 * rows * cols)
            ifftFilterArray[s] = ifftFilt

            complex_filter = tf.cast(filter, dtype=tf.complex64)
            inverse_fft2d = ifft2(imagefft * complex_filter)
            EO[s][o] = inverse_fft2d
            An = tf.math.abs(inverse_fft2d)
            sumAn_ThisOrient = sumAn_ThisOrient + An
            sumE_ThisOrient = sumE_ThisOrient + tf.math.real(inverse_fft2d)
            sumO_ThisOrient = sumO_ThisOrient + tf.math.imag(inverse_fft2d)
            maxAn = An if s == 0 else tf.math.maximum(maxAn, An)
            EM_n = tf.reduce_sum(filter ** 2) if s == 0 else EM_n
        sqrtXEnergy = tf.math.sqrt(sumE_ThisOrient ** 2 + sumO_ThisOrient ** 2)
        XEnergy = sqrtXEnergy + epsilon
        MeanE = sumE_ThisOrient / XEnergy
        MeanO = sumO_ThisOrient / XEnergy

        for s in range(nscale):
            E = tf.math.real(EO[s][o])
            O = tf.math.imag(EO[s][o])
            Energy = Energy + E * MeanE + O * MeanO - \
                tf.math.abs(E * MeanO - O * MeanE)
        # TODO remove square of abs
        square_absolute = tf.math.abs(EO[0][o]) ** 2  # TODO rm maximum square
        medianE2n = median(tf.reshape(square_absolute, [-1]))
        meanE2n = -medianE2n/tf.math.log(0.5)
        noisePower = meanE2n/EM_n

        EstSumAn2 = zero
        for s in range(nscale):
            EstSumAn2 = EstSumAn2 + ifftFilterArray[s] ** 2

        EstSumAiAj = zero
        for si in range(nscale-1):
            for sj in range(si+1, nscale):
                EstSumAiAj = EstSumAiAj + \
                    ifftFilterArray[si] * ifftFilterArray[sj]
        sumEstSumAn2 = tf.reduce_sum(EstSumAn2)
        sumEstSumAiAj = tf.reduce_sum(EstSumAiAj)

        EstNoiseEnergy2 = 2 * noisePower * sumEstSumAn2 + \
            4 * noisePower * sumEstSumAiAj

        tau = tf.sqrt(EstNoiseEnergy2 / 2.0)

        EstNoiseEnergy = tau * tf.sqrt(pi/2)
        EstNoiseEnergySigma = tf.sqrt((2-pi/2) * tau * tau)

        T = EstNoiseEnergy + k*EstNoiseEnergySigma
        T = T / 1.7  # Empirical rescaling

        Energy = tf.math.maximum(Energy - T, zero)

        EnergyAll = EnergyAll + Energy

        AnAll = AnAll + sumAn_ThisOrient

    ResultPC = EnergyAll / AnAll

    return ResultPC


def FeatureSIM(imageRef, imageDis):
    d = imageRef.get_shape().as_list()
    rows, cols = d[0], d[1]
    I1 = tf.ones((rows, cols))
    I2 = tf.ones((rows, cols))
    Q1 = tf.ones((rows, cols))
    Q2 = tf.ones((rows, cols))

    Y1 = imageRef
    Y2 = imageDis

    if len(d) == 3:
        Y1 = 0.299 * imageRef[:, :, 0] + 0.587 * \
            imageRef[:, :, 1] + 0.114 * imageRef[:, :, 2]
        Y2 = 0.299 * imageDis[:, :, 0] + 0.587 * \
            imageDis[:, :, 1] + 0.114 * imageDis[:, :, 2]
        I1 = 0.596 * imageRef[:, :, 0] - 0.274 * \
            imageRef[:, :, 1] - 0.322 * imageRef[:, :, 2]
        I2 = 0.596 * imageDis[:, :, 0] - 0.274 * \
            imageDis[:, :, 1] - 0.322 * imageDis[:, :, 2]
        Q1 = 0.211 * imageRef[:, :, 0] - 0.523 * \
            imageRef[:, :, 1] + 0.312 * imageRef[:, :, 2]
        Q2 = 0.211 * imageDis[:, :, 0] - 0.523 * \
            imageDis[:, :, 1] + 0.312 * imageDis[:, :, 2]

    Y1 = tf.cast(Y1, dtype=tf.float32)
    Y2 = tf.cast(Y2, dtype=tf.float32)

    ####
    # Downsample the image
    ####
    minDimension = tf.math.minimum(rows, cols)
    minDimension = tf.cast(minDimension,
                           dtype=tf.float64) / tf.cast(256.0, dtype=tf.float64)
    minDimension = tf.math.round(minDimension)
    F = tf.math.maximum(tf.cast(1.0, dtype=tf.float64), minDimension)
    aveKernel = fspecial('average', F)

    F = tf.cast(F, dtype=tf.int32)

    aveI1 = conv2(I1, aveKernel, 'same')
    aveI2 = conv2(I2, aveKernel, 'same')
    I1 = aveI1[0:rows:F, 0:cols:F]
    I2 = aveI2[0:rows:F, 0:cols:F]

    aveQ1 = conv2(Q1, aveKernel, 'same')
    aveQ2 = conv2(Q2, aveKernel, 'same')
    Q1 = aveQ1[0:rows:F, 0:cols:F]
    Q2 = aveQ2[0:rows:F, 0:cols:F]

    aveY1 = conv2(Y1, aveKernel, 'same')
    aveY2 = conv2(Y2, aveKernel, 'same')
    Y1 = aveY1[0:rows:F, 0:cols:F]
    Y2 = aveY2[0:rows:F, 0:cols:F]

    ####
    # Calculate the phase congruency maps
    ####
    PC1 = phasecong2(Y1)
    PC2 = phasecong2(Y2)

    ####
    # Calculate the gradient map
    ####
    dx = tf.constant(
        [[0.1875, 0, -0.1875],
         [0.6250, 0, -0.6250],
         [0.1875, 0, -0.1875]])
    dy = tf.constant(
        [[0.1875, 0.6250, 0.1875],
         [0, 0, 0],
         [-0.1875, -0.6250, -0.1875]])
    IxY1 = conv2(Y1, dx, 'same')
    IyY1 = conv2(Y1, dy, 'same')
    gradientMap1 = tf.sqrt(IxY1 ** 2 + IyY1 ** 2)

    IxY2 = conv2(Y2, dx, 'same')
    IyY2 = conv2(Y2, dy, 'same')
    gradientMap2 = tf.sqrt(IxY2 ** 2 + IyY2 ** 2)

    ####
    # Calculate the FSIM
    ####
    T1, T2 = 0.85, 160
    PCSimMatrix = (2 * PC1 * PC2 + T1) / (PC1 ** 2 + PC2 ** 2 + T1)
    gradientSimMatrix = (2*gradientMap1 * gradientMap2 + T2) / \
        (gradientMap1 ** 2 + gradientMap2 ** 2 + T2)
    PCm = tf.math.maximum(PC1, PC2)
    SimMatrix = gradientSimMatrix * PCSimMatrix * PCm
    FSIM = tf.reduce_sum(SimMatrix) / tf.reduce_sum(PCm)

    ####
    # Calculate the FSIMc
    ####
    T3 = 200
    T4 = 200
    ISimMatrix = (2 * I1 * I2 + T3) / (I1 ** 2 + I2 ** 2 + T3)
    QSimMatrix = (2 * Q1 * Q2 + T4) / (Q1 ** 2 + Q2 ** 2 + T4)

    lambda_L = 0.03

    SimMatrixC = gradientSimMatrix * PCSimMatrix * \
        tf.math.real((ISimMatrix * QSimMatrix) ** lambda_L) * PCm
    FSIMc = tf.reduce_sum(SimMatrixC) / tf.reduce_sum(PCm)

    return tf.squeeze(FSIM), tf.squeeze(FSIMc)

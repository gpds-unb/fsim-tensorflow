clear; clc;

CSV=readtable('./live2.csv', 'Delimiter',',');

test_location = CSV.LOCATION;
test_names = CSV.SIGNAL;

ref_location = CSV.REFLOCATION;
ref_names = CSV.REF;


FSIMs = [];
FSIMcs = [];

for i=1:length(test_location)
    ref_path = strcat(ref_location{i}, '/', ref_names{i});
    test_path = strcat(test_location{i}, '/', test_names{i});
    
    ref = imread(ref_path);
    test = imread(test_path);
    
    [FSIM, FSIMc] = FeatureSIM(ref, test);
    
    disp(sprintf('%.3f,%.3f', FSIM, FSIMc))
end
